import React, { useState, useEffect, useContext } from "react";
import Router from "next/router";
import styles from "./../styles/index.module.css";
import NavBar from "../components/NavBar";
import { Container, Row, Col } from "react-bootstrap";

import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { GoogleLogin } from "react-google-login";

export default function Home() {
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(true);

	function authenticate(e) {
		e.preventDefault();

		fetch("https://arcane-oasis-83959.herokuapp.com/api/users/login", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then(res => res.json())
			.then(data => {
				if (data.accessToken) {
					localStorage.setItem("token", data.accessToken);

					fetch(
						"https://arcane-oasis-83959.herokuapp.com/api/users/details",
						{
							headers: {
								Authorization: `Bearer ${data.accessToken}`,
							},
						}
					)
						.then(res => res.json())
						.then(data => {
							localStorage.setItem("id", data._id);
							localStorage.setItem("isAdmin", data.isAdmin);

							setUser({
								id: data._id,
							});
						});

					setEmail("");
					setPassword("");

					Swal.fire({
						icon: "success",
						title: "Successfully logged in.",
						text: "Thank you for logging in.",
					});

					Router.push("/records");
				} else {
					Swal.fire({
						icon: "error",
						title: "Unsuccessful.",
						text: "User authentication failed.",
					});
				}
			});
	}

	useEffect(() => {
		if (email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	function authenticateGoogleToken(response) {
		fetch(
			"https://arcane-oasis-83959.herokuapp.com/api/users/verify-google-id-token",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					tokenId: response.tokenId,
				}),
			}
		)
			.then(res => res.json())
			.then(data => {
				if (typeof data.accessToken !== "undefined") {
					localStorage.setItem("token", data.accessToken);
					retrieveUserDetails(data.accessToken);
					Router.push("/records");
				} else {
					if (data.error === "google-auth-error") {
						Swal.fire(
							"Google Auth Error",
							"Google authentication procedure failed",
							"error"
						);
					} else if (data.error === "login-type-error") {
						Swal.fire(
							"Login Type Error",
							"You may have registered through a different login procedure",
							"error"
						);
					}
				}
			});
	}

	function retrieveUserDetails(accessToken) {
		fetch("https://arcane-oasis-83959.herokuapp.com/api/users/details", {
			headers: { Authorization: `Bearer ${accessToken}` },
		})
			.then(res => res.json())
			.then(data => {
				localStorage.setItem("id", data._id);

				setUser({
					id: data._id,
				});
			});
	}
	return (
		<React.Fragment>
			<div className={styles.main_background}>
				<div className={styles.login_register_box}>
					<Form onSubmit={e => authenticate(e)}>
						<h1 className={styles.title_design}>SALA&#8369;I</h1>
						<p className={styles.phrase}>
							Manage your personal finances
						</p>
						<Form.Group controlId="userEmail">
							<Form.Control
								type="email"
								placeholder="Email Address"
								value={email}
								onChange={e => setEmail(e.target.value)}
								required
								className={styles.input_design}
							/>
						</Form.Group>
						<Form.Group controlId="password">
							<Form.Control
								type="password"
								placeholder="Password"
								value={password}
								onChange={e => setPassword(e.target.value)}
								required
								className={styles.input_design}
							/>
						</Form.Group>
						{isActive ? (
							<Button
								variant="warning"
								type="submit"
								id="submitBtn"
								className={styles.button}
							>
								Login
							</Button>
						) : (
							<Button
								variant="warning"
								type="submit"
								id="submitBtn"
								disabled
								className={styles.button}
							>
								Login
							</Button>
						)}
						<GoogleLogin
							clientId="1068033483350-bd7oci9me9237ud9sagpv034788r0u9t.apps.googleusercontent.com"
							buttonText="Login Using Google"
							onSuccess={authenticateGoogleToken}
							onFailure={authenticateGoogleToken}
							cookiePolicy={"single_host_origin"}
							className={styles.google_button}
						/>
						<p className={styles.to_login_register}>
							If you haven't registered yet,{" "}
							<a
								href="/register"
								className={styles.login_register_link}
							>
								Sign Up Here!
							</a>
						</p>
					</Form>
				</div>
			</div>
		</React.Fragment>
	);
}
