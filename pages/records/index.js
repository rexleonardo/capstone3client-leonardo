import React, { useEffect, useState, useContext } from "react";
import {
	InputGroup,
	Form,
	FormControl,
	Button,
	Card,
	Container,
	Row,
	Col,
} from "react-bootstrap";
import NavBar from "./../../components/NavBar";
import styles from "./../../styles/index.module.css";

export default function Records() {
	const [transactions, setTransactions] = useState([]);
	const [savings, setSavings] = useState([]);
	const [expenses, setExpenses] = useState([]);
	const [incomes, setIncomes] = useState([]);
	const [type, setType] = useState("All");

	useEffect(() => {
		let token = localStorage.getItem("token");
		fetch("https://arcane-oasis-83959.herokuapp.com/api/records/", {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})
			.then(res => res.json())
			.then(data => {
				setTransactions(data);

				data.map(data => {
					if (data.type === "Income") {
						setIncomes(incomes => [...incomes, data]);
					} else {
						setExpenses(expenses => [...expenses, data]);
					}
				});

				fetch(
					"https://arcane-oasis-83959.herokuapp.com/api/users/details/",
					{
						headers: {
							Authorization: `Bearer ${token}`,
						},
					}
				)
					.then(res => res.json())
					.then(data => {
						setSavings(data.savings);
					});
			});
	}, []);

	const transaction = transactions
		.slice(0)
		.reverse()
		.map(data => {
			// const ListOfCurrentTransaction = transactions.filter(value => value.dateOfTransaction <= data.dateOfTransaction && value.type === data.type);

			let dateObject = new Date(data.createdON);
			let dateString = JSON.stringify(dateObject.toUTCString());
			dateString = dateString.substring(1, dateString.length - 13);

			if (data.type === "Income") {
				// const totalIncome = ListOfCurrentTransaction.reduce((a, b) => +a + +b.amount, 0);

				return (
					<Card key={data._id}>
						<Card.Body>
							<Row>
								<Col className="col-6">
									<h5>{data.description}</h5>
									<h6>
										<span className="text-success">
											Income
										</span>{" "}
										{/* ({data.name}) */}
									</h6>
									<p>{dateString}</p>
								</Col>
								<Col className="col-6 text-right">
									<h6 className="text-success">
										+ &#8369; {data.amount.toLocaleString()}
									</h6>
									{/* <span className="text-success">{savings}</span> */}
								</Col>
							</Row>
						</Card.Body>
					</Card>
				);
			} else {
				// const totalExpense = ListOfCurrentTransaction.reduce((a, b) => +a + +b.amount, 0);

				return (
					<Card key={data._id}>
						<Card.Body>
							<Row>
								<Col className="col-6">
									<h5>{data.description}</h5>
									<h6>
										<span className="text-danger">
											Expense
										</span>{" "}
										{/* ({data.name}) */}
									</h6>
									<p>{dateString}</p>
								</Col>
								<Col className="col-6 text-right">
									<h6 className="text-danger">
										- &#8369; {data.amount.toLocaleString()}
									</h6>
									{/* <span className="text-danger">{savings}</span> */}
								</Col>
							</Row>
						</Card.Body>
					</Card>
				);
			}
		});

	let income = incomes
		.slice(0)
		.reverse()
		.map(data => {
			let dateObject = new Date(data.createdON);
			let dateString = JSON.stringify(dateObject.toUTCString());
			dateString = dateString.substring(1, dateString.length - 13);

			return (
				<Card key={data._id}>
					<Card.Body>
						<Row>
							<Col className="col-6">
								<h5>{data.description}</h5>
								<h6>
									<span className="text-success">Income</span>
								</h6>
								<p>{dateString}</p>
							</Col>
							<Col className="col-6 text-right">
								<h6 className="text-success">
									+ &#8369; {data.amount.toLocaleString()}
								</h6>
							</Col>
						</Row>
					</Card.Body>
				</Card>
			);
		});

	let expense = expenses
		.slice(0)
		.reverse()
		.map(data => {
			let dateObject = new Date(data.createdON);
			let dateString = JSON.stringify(dateObject.toUTCString());
			dateString = dateString.substring(1, dateString.length - 13);

			return (
				<Card key={data._id}>
					<Card.Body>
						<Row>
							<Col className="col-6">
								<h5>{data.description}</h5>
								<h6>
									<span className="text-danger">Expense</span>
								</h6>
								<p>{dateString}</p>
							</Col>
							<Col className="col-6 text-right">
								<h6 className="text-danger">
									{" "}
									- &#8369; {data.amount.toLocaleString()}
								</h6>
							</Col>
						</Row>
					</Card.Body>
				</Card>
			);
		});

	return (
		<React.Fragment>
			<NavBar />
			<Container className={styles.top_bottom_padding}>
				<Row className={styles.center}>
					<Col className={styles.max_width}>
						<div className={styles.bottom_padding}>
							<h1>Records</h1>
							<h6>
								Total Savings: &#8369;{" "}
								{savings.toLocaleString()}
							</h6>
						</div>
						<InputGroup className={styles.bottom_padding}>
							<InputGroup.Prepend>
								<Button
									variant="warning"
									type="submit"
									id="submitBtn"
									href="/records/add"
								>
									Add
								</Button>
							</InputGroup.Prepend>
							<FormControl placeholder="Search Record" />
							<Form.Control
								as="select"
								onChange={e => setType(e.target.value)}
								required
							>
								<option>All</option>
								<option>Income</option>
								<option>Expense</option>
							</Form.Control>
						</InputGroup>

						{type == "All"
							? transaction
							: type == "Income"
							? income
							: expense}
					</Col>
				</Row>
			</Container>
		</React.Fragment>
	);
}
