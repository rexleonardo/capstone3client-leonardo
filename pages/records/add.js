import React, { useState, useContext, useEffect } from "react";
import { Card, Button, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import Router from "next/router";
import NavBar from "./../../components/NavBar";

export default function add() {
	const [all, setAll] = useState("");
	const [income, setIncome] = useState("");
	const [expense, setExpense] = useState("");
	const [type, setType] = useState("");
	const [category, setCategory] = useState("");
	const [name, setName] = useState("");
	const [amount, setAmount] = useState("");
	const [description, setDescription] = useState("");
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		if (name !== "" && type !== "" && amount !== "" && description !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, type, amount, description]);

	useEffect(() => {
		fetch(
			"https://arcane-oasis-83959.herokuapp.com/api/categories/details",
			{
				headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
			}
		)
			.then(res => res.json())
			.then(data => {
				setAll(
					data.map(res => {
						return { name: res.name, type: res.type };
					})
				);

				let incomeList = data.filter(res => {
					if (res.type === "Income") {
						return res;
					}
				});

				let expenseList = data.filter(res => {
					if (res.type === "Expense") {
						return res;
					}
				});

				setIncome(
					incomeList.map(res => {
						return { name: res.name };
					})
				);

				setExpense(
					expenseList.map(res => {
						return { name: res.name };
					})
				);
			});
	}, []);

	useEffect(() => {
		function categoryNames(result) {
			return result.map(e => {
				return (
					<option value={e.name} key={e.name}>
						{e.name}
					</option>
				);
			});
		}
		if (type === "Income") {
			setCategory(categoryNames(income));
		}
		if (type === "Expense") {
			setCategory(categoryNames(expense));
		}

		setName("");
	}, [type]);

	function addRecord(e) {
		e.preventDefault();

		fetch("https://arcane-oasis-83959.herokuapp.com/api/records/", {
			method: "POST",
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				name: name,
				type: type,
				amount: amount,
				description: description,
			}),
		})
			.then(res => res.json())
			.then(data => {
				if (data) {
					fetch(
						"https://arcane-oasis-83959.herokuapp.com/api/users/savingsUpdate",
						{
							method: "POST",
							headers: {
								"Content-Type": "application/json",
								Authorization: `Bearer ${localStorage.getItem(
									"token"
								)}`,
							},
						}
					)
						.then(res => res.json())
						.then(data => {
							if (data === true) {
								Swal.fire({
									icon: "success",
									title: "Record Added",
								});
								Router.push("/records");
							} else {
								Swal.fire({
									icon: "error",
									title: "Add Record Failed!",
								});
							}
						});
				} else {
					Swal.fire({
						icon: "error",
						title: "Add Record Failed!",
					});
				}
			});
	}

	function addRecord(e) {
		e.preventDefault();

		let token = localStorage.getItem("token");
		fetch("https://arcane-oasis-83959.herokuapp.com/api/records/", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				name: name,
				type: type,
				amount: amount,
				description: description,
			}),
		})
			.then(res => res.json())
			.then(data => {
				console.log(`data from addREcord ${data}`);
				if (data) {
					fetch(
						"https://arcane-oasis-83959.herokuapp.com/api/users/savingsUpdate",
						{
							method: "POST",
							headers: {
								"Content-Type": "application/json",
								Authorization: `Bearer ${localStorage.getItem(
									"token"
								)}`,
							},
						}
					)
						.then(res => res.json())
						.then(data => {
							console.log(`data from update ${data}`);
							if (data) {
								Swal.fire({
									icon: "success",
									title: "Record Saved",
									text: "Thank you for adding record.",
								});
								Router.push("/records");
							} else {
								Swal.fire({
									icon: "error",
									title: "Please Try Again",
									text: "Failed adding record.",
								});
							}
						});
				} else {
					Swal.fire({
						icon: "error",
						title: "Please Try Again",
						text: "Failed adding record.",
					});
				}
			});
		setName("");
		setType("");
		setAmount("");
		setDescription("");
	}

	return (
		<React.Fragment>
			<NavBar />
			<h1 className="col-md-12 col-lg-6">New Record</h1>
			<Card>
				<Card.Header>Record Information</Card.Header>
				<Card.Body>
					<Form onSubmit={e => addRecord(e)}>
						<Form.Group controlId="type">
							<Form.Control
								as="select"
								value={type}
								onChange={e => setType(e.target.value)}
								required
							>
								<option value="" disabled>
									Category Type
								</option>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
							</Form.Control>
						</Form.Group>
						<Form.Group controlId="name">
							<Form.Control
								as="select"
								value={name}
								onChange={e => setName(e.target.value)}
								required
							>
								<option value="" disabled>
									Category Name
								</option>
								{category}
							</Form.Control>
						</Form.Group>
						<Form.Group controlId="amount">
							<Form.Control
								type="number"
								placeholder="Amount"
								value={amount}
								onChange={e => setAmount(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group controlId="description">
							<Form.Control
								type="text"
								placeholder="Description"
								value={description}
								onChange={e => setDescription(e.target.value)}
								required
							/>
						</Form.Group>
						{isActive ? (
							<Button
								variant="warning"
								type="submit"
								id="submitBtn"
								className="btn-block"
							>
								Submit
							</Button>
						) : (
							<Button
								variant="warning"
								type="submit"
								id="submitBtn"
								className="btn-block"
								disabled
							>
								Submit
							</Button>
						)}
					</Form>
				</Card.Body>
			</Card>
		</React.Fragment>
	);
}
