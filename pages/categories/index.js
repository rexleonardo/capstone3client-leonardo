import React, { useEffect, useState, useContext } from "react";
import { Table, Button } from "react-bootstrap";
import NavBar from "./../../components/NavBar";

export default function Categories() {
	const [categories, setCategories] = useState([]);

	useEffect(() => {
		let token = localStorage.getItem("token");
		fetch(
			"https://arcane-oasis-83959.herokuapp.com/api/categories/details",
			{
				headers: {
					Authorization: `Bearer ${token}`,
				},
			}
		)
			.then(res => res.json())
			.then(data => {
				setCategories(data);
			});
	}, []);

	const categoryList = categories.map(category => {
		return (
			<tr key={category._id}>
				<td>{category.name}</td>
				<td>{category.type}</td>
			</tr>
		);
	});

	return (
		<React.Fragment>
			<NavBar />
			<h1>Categories</h1>
			<Button
				className="my-3"
				type="submit"
				variant="warning"
				href="/categories/add"
			>
				Add Category
			</Button>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Category</th>
						<th>Type</th>
					</tr>
				</thead>
				<tbody>{categoryList}</tbody>
			</Table>
		</React.Fragment>
	);
}
