import React, { useState, useEffect } from "react";
import { Button, Card, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import Router from "next/router";
import NavBar from "./../../components/NavBar";

export default function categories() {
	const [name, setName] = useState("");
	const [type, setType] = useState("");
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		if (name !== "" && type !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, type]);

	function addCategory(e) {
		e.preventDefault();

		let token = localStorage.getItem("token");
		fetch("https://arcane-oasis-83959.herokuapp.com/api/categories/add", {
			method: "POST",
			headers: {
				Authorization: `Bearer ${token}`,
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				name: name,
				type: type,
			}),
		})
			.then(res => res.json())
			.then(data => {
				if (data) {
					Swal.fire({
						icon: "success",
						title: "Category added!",
					});

					Router.push("/categories");
				} else {
					Swal.fire({
						icon: "error",
						title: "Add category failed!",
						text: "There has been an internal server error.",
					});
				}
			});

		setName("");
		setType("");
	}

	return (
		<React.Fragment>
			<NavBar />
			<h1>New Category</h1>
			<Card>
				<Card.Header>Category Information</Card.Header>
				<Card.Body>
					<Form onSubmit={e => addCategory(e)}>
						<Form.Group>
							<Form.Control
								type="text"
								value={name}
								onChange={e => setName(e.target.value)}
								placeholder="Category Name"
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Control
								as="select"
								value={type}
								onChange={e => setType(e.target.value)}
								required
							>
								<option disabled value="">
									Category Type
								</option>
								<option>Income</option>
								<option>Expense</option>
							</Form.Control>
						</Form.Group>
						{isActive ? (
							<Button
								variant="warning"
								type="submit"
								id="submitBtn"
								className="btn-block"
							>
								Submit
							</Button>
						) : (
							<Button
								variant="warning"
								type="submit"
								id="submitBtn"
								className="btn-block"
								disabled
							>
								Submit
							</Button>
						)}
					</Form>
				</Card.Body>
			</Card>
		</React.Fragment>
	);
}
