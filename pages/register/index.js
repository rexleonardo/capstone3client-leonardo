import React, { useState, useEffect } from "react";
import Router from "next/router";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import styles from "./../../styles/index.module.css";

export default function register() {
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {
		e.preventDefault();

		fetch("https://arcane-oasis-83959.herokuapp.com/api/users/emailExist", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then(res => res.json())
			.then(data => {
				if (data) {
					return Swal.fire({
						icon: "error",
						title: "Registration Failed.",
						text: "Email has already been registered.",
					});
				} else {
					fetch(
						"https://arcane-oasis-83959.herokuapp.com/api/users/register",
						{
							method: "POST",
							headers: {
								"Content-Type": "application/json",
							},
							body: JSON.stringify({
								firstName: firstName,
								lastName: lastName,
								email: email,
								mobileNo: mobileNo,
								password: password1,
								confirmPassword: password2,
							}),
						}
					)
						.then(res => res.json())
						.then(data => {
							console.log(data);
							Swal.fire({
								icon: "success",
								title: "Registration Successful!",
								text: "Thank you for registering.",
							});
							setFirstName("");
							setLastName("");
							setEmail("");
							setMobileNo("");
							setPassword1("");
							setPassword2("");
							Router.push("/");
						});
				}
			});
	}

	useEffect(() => {
		if (
			firstName !== "" &&
			lastName !== "" &&
			mobileNo !== "" &&
			email !== "" &&
			password1 !== "" &&
			password2 !== "" &&
			password1 === password2 &&
			mobileNo.length === 11
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password1, password2]);

	return (
		<React.Fragment>
			<div className={styles.main_background}>
				<div className={styles.login_register_box}>
					<Form onSubmit={e => registerUser(e)}>
						<h1 className={styles.title_design}>Register</h1>
						<p className={styles.phrase}>Create your account</p>
						<Form.Group controlId="firstName">
							<Form.Control
								type="text"
								placeholder="First Name"
								value={firstName}
								onChange={e => setFirstName(e.target.value)}
								required
								className={styles.input_design}
							/>
						</Form.Group>
						<Form.Group controlId="lastName">
							<Form.Control
								type="text"
								placeholder="Last Name"
								value={lastName}
								onChange={e => setLastName(e.target.value)}
								required
								className={styles.input_design}
							/>
						</Form.Group>
						<Form.Group controlId="mobileNo">
							<Form.Control
								type="number"
								placeholder="Mobile Number"
								value={mobileNo}
								onChange={e => setMobileNo(e.target.value)}
								required
								className={styles.input_design}
							/>
						</Form.Group>
						<Form.Group controlId="userEmail">
							<Form.Control
								type="email"
								placeholder="Email Address"
								value={email}
								onChange={e => setEmail(e.target.value)}
								required
								className={styles.input_design}
							/>
						</Form.Group>
						<Form.Group controlId="password1">
							<Form.Control
								type="password"
								placeholder="Password"
								value={password1}
								onChange={e => setPassword1(e.target.value)}
								required
								className={styles.input_design}
							/>
						</Form.Group>
						<Form.Group controlId="password2">
							<Form.Control
								type="password"
								placeholder="Verify Password"
								value={password2}
								onChange={e => setPassword2(e.target.value)}
								required
								className={styles.input_design}
							/>
						</Form.Group>
						{isActive ? (
							<Button
								variant="warning"
								type="submit"
								id="submitBtn"
								className={styles.button}
							>
								Register
							</Button>
						) : (
							<Button
								variant="warning"
								type="submit"
								id="submitBtn"
								className={styles.button}
								disabled
							>
								Register
							</Button>
						)}
						<p className={styles.to_login_register}>
							Already have an account?{" "}
							<a href="/" className={styles.login_register_link}>
								Login here.
							</a>
						</p>
					</Form>
				</div>
			</div>
		</React.Fragment>
	);
}
