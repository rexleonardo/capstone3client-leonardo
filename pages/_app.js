import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";

import React, { useState, useEffect } from "react";
import Head from "next/head";
import NavBar from "../components/NavBar";
import { Container } from "react-bootstrap";
import { UserProvider } from "../UserContext";
import styles from "./../styles/index.module.css";

function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
	});

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null,
			isAdmin: null,
		});
	};

	useEffect(() => {
		setUser({
			id: localStorage.getItem("id"),
			isAdmin: localStorage.getItem("isAdmin") === "true",
		});
	}, []);

	return (
		<React.Fragment>
			<UserProvider value={{ user, setUser, unsetUser }}>
				<Head>
					<link rel="preconnect" href="https://fonts.gstatic.com" />
					<link
						href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
						rel="stylesheet"
					/>
				</Head>
				{/* <NavBar /> */}
				<Container fluid className={styles.container_padding}>
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</React.Fragment>
	);
}

export default MyApp;
